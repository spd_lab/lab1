import numpy as np

def ProcessData(data):
    data = np.genfromtxt('SCHRAGE1.DAT',

                     skip_header=1,
                     dtype=int,
                     delimiter="")

    return data




def cmax(r,p,q,pi,n):
    t=0
    u=0
    for i in range(0,n-1):
        j=pi[i]
        t=max(t,r[j]+p[j])
        u=max(u, t+q[j])
    return u


def main():

    data = np.array
    k = ProcessData(data).tolist()
    k.sort()
    d = np.asmatrix(k)
    print(cmax(d[:,0], d[:,1],d[:,2],range(1,d.shape[0]), d.shape[0]))

if _name_ == "_main_":
    main()