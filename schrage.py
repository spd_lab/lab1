import numpy as np
import sys

def main():

	data = np.array
	k = ProcessData(data)

	r = toSingleMatrix(k[:, 0])
	p = toSingleMatrix(k[:, 1])
	q = toSingleMatrix(k[:, 2])


	#print findMaximum(q[:])

	print(schrageAlgorithm(r[:], p[:], q[:]))

def toSingleMatrix(list):
	result = []

	for i in range(0, len(list)):
		result.append(list[i])
	return result

def ProcessData(data):
    data = np.genfromtxt('SCHRAGE2.DAT',

                     skip_header=1,
                     dtype=int,
                     delimiter="")

    return data

def schrageAlgorithm(r, p, q):
	t = 0
	k = 0
	Cmax = 0

	N = [] 
	N_size = 0
	G = []
	PI = []

	for i in range(0, len(r)):
		N.append(1);

	N_size = len(N)

	while(len(G)!=0 or N_size!=0):

		minimum = sys.maxsize
		argmin = -1

		for i in range(0, len(r)):
			if(N[i] == 1 and r[i] < minimum):
				minimum = r[i]
				argmin = i

		while(N_size!=0 and minimum <= t):
			N[argmin] = -1
			N_size-=1
			G.append((q[argmin], argmin))

			minimum = sys.maxsize

			for i in range(0, len(r)):
				if(N[i] == 1 and r[i] < minimum):
					minimum = r[i]
					argmin = i

		if(len(G)==0):
			t = minimum
		else:
			maximum = -1
			maxID = -1  
			ind = -1
			for i in range(0, len(G)):
				A = G[i]
				print "A: "+str(A[0])+" "+str(A[1])
				if maximum<A[0]:
					maximum = A[0]
					maxID = A[1]
					ind = i

			G.remove(G[ind])
			#print "Adding to PI: ("+str(maximum)+", "+str(maxID)+")"
			PI.append(maxID)

			t+= p[maxID]

			Cmax = max(Cmax, t+q[maxID])

	print "Cmax: "+str(Cmax)
	print "Queue: "
	print PI

main()