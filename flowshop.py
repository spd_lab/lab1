#!/usr/bin/env python

import sys


def main():
	machine1 = [14, 22, 6, 15, 26]
	machine2 = [1, 2, 1, 6, 5]
	machine3 = [7, 19, 6, 15, 4]
	#queue = [0, 2, 4, 3, 1]
	bruteforce(machine1[:], machine2[:], machine3[:])
	print "Johnsons: "+str(flowshop3(machine1[:], machine2[:], machine3[:], johnsons(addMachinesTime(machine1, machine2), machine3)))
	print  str(johnsons(addMachinesTime(machine1, machine2), machine3))
def addMachinesTime(m1, m2):
	A = []
	for i in range(0, len(m1)):
		A.append(m1[i]+m2[i])
	return A

def permute(xs, low=0):
    if low + 1 >= len(xs):
        yield xs
    else:
        for p in permute(xs, low + 1):
            yield p        
        for i in range(low + 1, len(xs)):        
            xs[low], xs[i] = xs[i], xs[low]
            for p in permute(xs, low + 1):
                yield p        
            xs[low], xs[i] = xs[i], xs[low]

def bruteforce(machine1, machine2, machine3 = []):
	time = 0
	minTime = sys.maxsize
	for p in permute([0, 1, 2, 3, 4]):
		if(len(machine3)!=0):
			time = str(flowshop3(machine1[:], machine2[:], machine3[:], p))
		else:
			time = str(flowshop2(machine1[:], machine2[:], p))

		time = int(time)
		if time < minTime:
			minTime = time
			print p

	print "Minimal total time found by bruteforce: "+str(minTime)

def johnsons(machine1, machine2):
	queue = []
	for i in range(len(machine1)):
		queue.append(-1)

	for k in range(len(machine1)):
		smallest1=machine1[0]
		id1 = 0
		for i in range(len(machine1)):
			if (smallest1>machine1[i] and machine1[i] != -1) or smallest1 == -1:
				smallest1 = machine1[i]
				id1 = i

		smallest2 = machine2[0]
		id2 = 0
		for i in range(len(machine2)):
			if (smallest2>machine2[i] and machine2[i] != -1) or smallest2 == -1:
				smallest2 = machine2[i]
				id2 = i

		if(smallest1<=smallest2):
			machine1[id1] = -1
			machine2[id1] = -1

			for i in range(len(machine1)):
				if queue[i] == -1:
					queue[i] = id1
					break
		else:
			machine1[id2] = -1
			machine2[id2] = -1
			for i in range(len(machine2)-1, -1, -1):
				if(queue[i] == -1):
					queue[i] = id2
					break



	return queue


def flowshop2(machine1time, machine2time, queue):
	time = 1
	machine1 = -1
	machine2 = -1
	currentTimeM1 = 0
	currentTimeM2 = 0

	while (machine1<len(queue) or machine2<len(queue)):
		if currentTimeM1 == 0:
			if machine1 != len(queue)-1:
				machine1+=1
				currentTimeM1 = machine1time[queue[machine1]]
			else:
				machine1+=1

		if currentTimeM2 <= 0:
			if (machine1 > machine2 + 1 or (machine1 == machine2+1 and currentTimeM1 <= 0)):
				if machine2+1 != len(queue):
					machine2+=1
					currentTimeM2 = machine2time[queue[machine2]]
				else:
					machine2+=1


		currentTimeM1-=1
		currentTimeM2-=1
		time+=1


	return time-2


def flowshop3(machine1time, machine2time, machine3time, queue):
	time = 1
	machine1 = -1
	machine2 = -1
	machine3 = -1
	currentTimeM1 = 0
	currentTimeM2 = 0
	currentTimeM3 = 0

	while (machine1<len(queue) or machine2<len(queue) or machine3<len(queue)):
		if currentTimeM1 == 0:
			if machine1 != len(queue)-1:
				machine1+=1
				currentTimeM1 = machine1time[queue[machine1]]
			else:
				machine1+=1

		if currentTimeM2 <= 0:
			if (machine1 > machine2 + 1 or (machine1 == machine2+1 and currentTimeM1 <= 0)):
				if machine2+1 != len(queue):
					machine2+=1
					currentTimeM2 = machine2time[queue[machine2]]
				else:
					machine2+=1

		if currentTimeM3 <= 0:
			if (machine2 > machine3+1 or (machine2 == machine3+1 and currentTimeM2 <= 0)):
				if machine3+1 != len(queue):
					machine3+=1
					currentTimeM3 = machine3time[queue[machine3]]
				else:
					machine3+=1


		currentTimeM1-=1
		currentTimeM2-=1
		currentTimeM3-=1
		time+=1


	return time-2


main()