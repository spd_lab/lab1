import numpy as np

def ProcessData(data):
    data = np.genfromtxt('SCHRAGE1.DAT',
                     skip_header=1,
                     dtype=int,
                     delimiter="")
    #print(data)

    return data

def permute(xs, low=0):
    if low + 1 >= len(xs):
        yield xs
    else:
        for p in permute(xs, low + 1):
            yield p
        for i in range(low + 1, len(xs)):
            xs[low], xs[i] = xs[i], xs[low]
            for p in permute(xs, low + 1):
                yield p
            xs[low], xs[i] = xs[i], xs[low]


def main():
    #print('problem RPQ - sortR')
    data = np.array
    k = ProcessData(data).tolist()
    #k.sort()
    #d = np.asmatrix(k)
    for p in permute(k):
        print(p)
if _name_ == "_main_":
    main()