import numpy as np

def ProcessData(data):
    data = np.genfromtxt('SCHRAGE1.DAT',
                     skip_header=1,
                     dtype=int,
                     delimiter="")
    #print(data)

    return data



def main():
    print('problem RPQ - sortR')
    data = np.array
    k = ProcessData(data).tolist()
    k.sort()
    d = np.asmatrix(k)
    print(d)
if _name_ == "_main_":
    main()